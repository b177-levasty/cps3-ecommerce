const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");

// Route for retrieving all orders
router.get("/all", auth.verify, (req, res) => {

	const userOrders = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.getAllOrders(userOrders).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all my orders
router.get("/myOrders", auth.verify, (req, res) => {

	const myOrders = auth.decode(req.headers.authorization).id;

	orderController.getMyOrders({userId: myOrders.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
