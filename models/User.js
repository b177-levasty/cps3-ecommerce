const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, `First Name is required`]
	},
	lastName : {
		type : String,
		required : [true, `Last Name is required`]
	},
	mobileNo : {
		type : String,
		required : [true, `Mobile number is required`]
	},
	email : {
		type : String,
		required : [true, `Email is required`]
	},
	password : {
		type : String,
		required : [true, `Password is required`]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	usersOrder :[
			{
				name : {
					type : String,
					required : [false, `Product Id is require`]
			},
				purchasedOn : {
					type : Date,
					default : new Date
			},
				status : {
					type : Boolean,
					default : true
			},
				price : {
					type: Number
				}
	}],
	addToCart :[
			{
				name : {
					type : String,
					required : [false, `Product Id is require`]
			},
				purchasedOn : {
					type : Date,
					default : new Date
			},
				status : {
					type : Boolean,
					default : true
			}
	}],

})

module.exports = mongoose.model("User", userSchema);