const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	PurchasedOn : {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		require: [true, "UserId is required"]
	},
	email: {
		type: String,
		require: [true, "Email is required"]
	},
	productId :{
		type: String,
		require: [true, "productId is required"]
	},
	name:{
		type: String,
		require: [true, "name is required"]
	},
	price : {
		type: Number,
		require: [true, "Price is required"]
	}
})

module.exports = mongoose.model("Order", orderSchema);


			  
				