const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register User
// Register Order
module.exports.registerAccount = (reqBody) => {

	let newAccount = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// let newCustomer = new Order ({
	// 	FirstName : reqBody.firstName,
	// 	LastName : reqBody.lastName,
	// 	MobileNo : reqBody.mobileNo
	// })

	// let saveBoth

	return newAccount.save().then((account, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Get account by using the userId
module.exports.getAccount = (data) => {

	return User.findById(data.userId).then(result => {		
		result.password = "";

		return result;

	});

};

// log-in account with authenication
module.exports.loginAccount = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}

		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}

			else{
				return false;
			}
		}
	})
}


// Set isAdmin to true by using the bearer token and the id of the user
module.exports.setAsAdmin = (data, reqParams) => {

	if(data.isAdmin) {
	let updateActiveField = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((course, error) => {

		if (error) {
			return false;

		} else {
			return true;

		}

	});
	}
	else {
  		return Promise.resolve(false);
	}
};

// For ordering
// dito HINDI PA
// module.exports.order = async (data, isAdmin) => {

// 		let isUserAccountUpdated = await User.findById(data.userId).then(user => {

// 			user.usersOrder.push({productId : data.productId}) 

// 			return user.save().then((user, error) => {
// 				if(error){
// 				return false;
// 				}
// 				else{
// 				return true;
// 				}
// 			})
// 		})
	
		
// 	let isProductUpdated = await Product.findById(data.productId).then(product => {
		
// 		product.customers.push({userId : data.userId})

// 		return product.save().then((product, error, req) => {
// 			if(error){
// 				return false;
// 			}
// 			else{
// 				return true;
// 			}
// 		})
// 	})


// 		if(isUserAccountUpdated && isProductUpdated){
// 				return true;
// 			}	
// 			else {
// 				return false;
// 			}
// 		}


// Changed
// module.exports.addToCart = async (data, isAdmin) => {

// 			let isUserAccountUpdated = await User.findById(data.userId).then(user => {

// 				user.addToCart.push({name : data.name}) 

// 				return user.save().then((user, error) => {
// 					if(error){
// 					return false;
// 					}
// 					else{
// 					return true;
// 					}
// 				})
// 			})
		
			
// 		let isProductUpdated = await Product.findById(data.productId).then(product => {
			
// 			product.cart.push({userId : data.userId})

// 			return product.save().then((product, error, req) => {
// 				if(error){
// 					return false;
// 				}
// 				else{
// 					return true;
// 				}
// 			})
// 		})

// 			if(isUserAccountUpdated && isProductUpdated){
// 					return true;
// 				}	
// 				else {
// 					return false;
// 				}
// 			}

module.exports.checkout = async (data) => {
	if(data.isAdmin){
		return Promise.resolve("Admins not allowed to create order");
	}
	else{
		// Add the product Id in the  array of the user
		let isUserUpdated = await User.findById(data.userId).then(user => {
			// Adds the productId in the user's purchases array
			user.usersOrder.push({name: data.name});

			// Save the updated user information in the database
			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Add the user Id in the buyers array of the product
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			//Adds the userId in the product's buyers array
			// product.buyers.push({userId: data.userId});
			product.customers.push({userId : data.userId})

			// Save the updated product information in the database
			return product.save().then((product, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		let isOrderUpdated = await Order.create().then(order => {
			//Adds the userId in the product's buyers array
			// Create a variable "newProduct" and instantiates a new "Product" object
			let newOrder = new Order({
				userId: data.userId,
				email: data.email,
				productId : data.productId,
				name: data.name, 
				price: data.price
			})

			// Save after instantiate
			return newOrder.save().then((order, error) => {
				if(error){
					return false;
				}
				else{
				return true;
				}
			})
		})

		// Condition that will check if the user and product documents have been updated 
		// User checkout is successful
		if(isUserUpdated && isProductUpdated && isOrderUpdated){
			return true;
		}
		// User checkout failure
		else{
			return false;
		}
	}
}

	



// For get all orders
// Okay na
// module.exports.getAllOrders = (data) => {

// 	if(data.isAdmin) {
// 		return User.find({__v: {$gte: 1}}).then(result => {
// 			return result;

// 			})
// 	}
// 	else {
// 		return Promise.resolve(false);
// 	}
// }
module.exports.getAllOrders = (userOrders) => {
	if(userOrders.isAdmin){
		return User.find({"usersOrder":{ $ne:[]}}, {email: 0, password: 0, isAdmin: 0}).then(result => {
			return result;
		})
	}
	else{
		return Promise.resolve(false);
	};	
}

module.exports.getMyOrders = (data) => {
	return User.findById(data.userId, {usersOrder : 1, _id: 0}).then(result => {
		return result;
	});
};

// For get users order

module.exports.getDetails = (data) => {

	if(data.isAdmin) {
		return false;
	
	}
	else {
		return User.findById(data.userId).then(result => {
			return result;
	});
  	
  }
};

// get all user

module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};

// module.exports.getMyOrders = (data) => {
// 	return User.findById(data.userId, {usersOrder : 1, _id: 0}).then(result => {
// 		return result;
// 	});
// };

// check email
module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return true; 

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		};
	});

};

