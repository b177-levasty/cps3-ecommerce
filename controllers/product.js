const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Create Product Sample Workflow
// okay na
module.exports.addProduct = (data) => {

	if (data.isAdmin) {

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {

			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} 

	else {
		return Promise.resolve(false);
	};
	
};

// Retrieve All Active Products Sample Workflow
// okay na
module.exports.getActiveProducts = () => {
        return Product.find({isActive: true}).then(result => {
        	return result;
        });
};

// Retrieve Single Product Sample Workflow
//okay na
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update Product Sample Workflow
// okay na
module.exports.updateProduct = (data, reqParams, reqBody) => {

	if(data.isAdmin) {

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		isActive : reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

			if(error){
				return false;
			}
			else{
				return true;
			}
	})
  }
  
  else {
  	return Promise.resolve(false);
  }
}

// Archive Product Sample Workflow
// okay na
module.exports.archiveProducts = (data, reqParams) => {

	if(data.isAdmin) {

	let archivedProducts = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProducts).then((product, error) => {

		if (error) {
			return false;
		} 

		else {
			return true;
		}

		});
	}
  	else {
  		return Promise.resolve(false);
  }
};

module.exports.activateProducts = (data, reqParams) => {

	if(data.isAdmin) {

	let archivedProducts = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProducts).then((product, error) => {

		if (error) {
			return false;
		} 

		else {
			return true;
		}

		});
	}
  	else {
  		return Promise.resolve(false);
  }
};

// get all products

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

/*
module.exports.archiveProducts = (data, reqParams, reqBody) => {

	if(data.isAdmin) {

	let archivedProducts = {
		isActive = reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProducts).then((product, error) => {

		if (error) {
			return false;

		} 
		else {
			return true;

		}

	});
  }
  else {
  		return Promise.resolve("Not an Admin");
  }
};
*/

/*module.exports.archiveProducts = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {

		if (error) {
			return false;

		} else {
			return true;

		}

	});
};
*/
/*
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};
*/