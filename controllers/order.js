// const Product = require("../models/Product");
// const User = require("../models/User");
// const Order = require("../models/Order");
// const bcrypt = require("bcrypt");
// const auth = require("../auth");

// module.exports.userOrders = async(data) => {

// 	if (data.isAdmin)  {

// 			let isUserDetails = await User.findById(data.userId).then(user => {
// 				// Adds the courseId in the user's enrollments array
// 				user.userOrders.push({
// 					userId : data.userId,
// 					email : data.email
// 				});

// 				// Save the updated user information
// 				return user.save().then((user, error) => {
// 					if(error){
// 						return false;
// 					}
// 					else{
// 						return true;
// 					}
// 				})
// 			})
// 			// Add the user Id in the enrollees array of the course
// 			let isProductDetails = await Product.findById(data.productId).then(product => {
// 				// Adds the userId in the course's enrollees array
// 				product.userOrders.push({
// 					productId : data.productId,
// 					name : data.name,
// 					price : data.price
// 				});

// 				// Saves the updated course information in the database
// 				return product.save().then((product, error) => {
// 					if(error){
// 						return false;
// 					}
// 					else{
// 						return true;
// 					}
// 				})
// 			})

// 			// Condition that will if the user and course documents have been updated
// 			// User enrollment is successful
// 			if(isUserDetails && isProductDetails){
// 				return true;
// 			}
// 			// User enrollment failure
// 			else{
// 				return false;
// 			}
// 		}
// 	}

const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const User = require("../models/User");

// Controller for getting all orders admin
module.exports.getAllOrders = (userOrders) => {
	if(userOrders.isAdmin){
		return Order.find().then(result => {
			return result;
		})
	}
	else{
		return Promise.resolve(false);
	};	
}

// Controller for getting all orders admin
module.exports.getMyOrders = (data) => {
	return User.find({userId: data.id}).then(result => {
		return result;
	});
};




